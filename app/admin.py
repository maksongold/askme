from django.contrib import admin

from .models import Answer
from .models import Profile
from .models import Question
from .models import Tag
from .models import AnswerLike
from .models import QuestionLike

admin.site.register(Profile)
admin.site.register(Tag)
admin.site.register(AnswerLike)
admin.site.register(QuestionLike)


class QuestionLikeInline(admin.TabularInline):
    model = QuestionLike
    extra = 0


class QuestionAdmin(admin.ModelAdmin):
    inlines = (QuestionLikeInline,)
    filter_horizontal = ('tags',)


class AnswerLikeInline(admin.TabularInline):
    model = AnswerLike
    extra = 0


class AnswerAdmin(admin.ModelAdmin):
    inlines = (AnswerLikeInline,)


admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)

# Register your models here.
