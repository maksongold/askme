from django.db import models
from datetime import date
from django.db.models import Count
from django.contrib.auth.models import User
import static

# TODO CHECK short_text


class ProfileManager(models.Manager):
    def popular(self):
        profiles = self.values('user__username')[:10]
        profiles_arr = []
        for profile in profiles:
            profiles_arr.append(profile.get('user__username'))
        return profiles_arr


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(default="unnamed.jpg", verbose_name="Аватар", upload_to='avatar/%Y/%m/%d')
    objects = ProfileManager()

    def __str__(self):
        return self.user.username


class QuestionManager(models.Manager):
    def new(self):
        return self.annotate(num_answers=Count('answer')).order_by('-date')

    def hot(self):
        return self.annotate(num_answers=Count('answer')).order_by('-rating')

    def one_question(self, question_id):
        return self.get(id=question_id)

    def one_tag(self, tag_name):
        return self.filter(tags__name=tag_name).annotate(num_answers=Count('answer'))


class TagManager(models.Manager):
    def popular(self):
        tags = self.values('question__tags__name').annotate(total=Count('name')).order_by('-total')[:20]
        tag_arr = []
        for tag in tags:
            tag_arr.append(tag.get('question__tags__name'))
        return tag_arr


class AnswerManager(models.Manager):
    def answers_for_question(self, question_id):
        return self.filter(question_id=question_id)


class Question(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    user_id = models.ForeignKey('Profile', on_delete=models.CASCADE)
    likes = models.ManyToManyField('Profile', through='QuestionLike', blank=True, related_name='likes_for_question')
    tags = models.ManyToManyField('Tag', blank=True)
    rating = models.IntegerField()
    date = models.DateField(default=date.today)
    objects = QuestionManager()

    def short_text(self):
        if len(self.text) > 130:
            return self.text[:130] + '...'
        else:
            return self.text

    def __str__(self):
        return self.text


class Answer(models.Model):
    text = models.TextField()
    question_id = models.ForeignKey('Question', on_delete=models.CASCADE)
    user_id = models.ForeignKey('Profile', on_delete=models.CASCADE)
    likes = models.ManyToManyField('Profile', through='AnswerLike', blank=True, related_name='likes_for_answer')
    rating = models.IntegerField()
    is_correct = models.BooleanField(default=False)
    objects = AnswerManager()

    def __str__(self):
        return self.text


class Tag(models.Model):
    name = models.CharField(max_length=40)
    index = models.Index('name', name='lower_name_tag_index')
    objects = TagManager()

    def __str__(self):
        return self.name


class QuestionLike(models.Model):
    user = models.ForeignKey("Profile", on_delete=models.CASCADE)
    question_id = models.ForeignKey("Question", on_delete=models.CASCADE)
    is_liked = models.BooleanField(default=True)

    class Meta:
        unique_together = [('user', 'question_id', 'is_liked'), ]

    def __str__(self):
        return self.user.user.username


class AnswerLike(models.Model):
    user = models.ForeignKey("Profile", on_delete=models.CASCADE)
    answer_id = models.ForeignKey("Answer", on_delete=models.CASCADE)
    is_liked = models.BooleanField(default=True)

    class Meta:
        unique_together = [('user', 'answer_id', 'is_liked'), ]

    def __str__(self):
        return self.user.user.username
