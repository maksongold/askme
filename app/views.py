from django.shortcuts import render
from django.core.paginator import Paginator
from app.models import *

from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.core.paginator import Paginator
from app.forms import *
from django.urls import reverse
from django.db.models import F

from django.core.cache import cache
from django.contrib import auth
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST

# paginator config
perr_page = 2
on_each_side = 1
on_ends = 1


def login(request):
    next_page = request.GET.get('next', default='/')
    if request.user.is_authenticated:
        return redirect(next_page)

    if request.method == 'GET':
        form = LoginForm()
        return render(request, 'login.html', {
            'form': form,
            'tags': Tag.objects.popular(),
            'members': Profile.objects.popular()
        })

    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            user = auth.authenticate(request,
                                     username=form.cleaned_data['username'],
                                     password=form.cleaned_data['password'],
                                     )
            if user is not None:
                auth.login(request, user)
                return redirect(next_page)
            else:
                form.add_error(None, 'Wrong password or login!')
        print(form.errors)
        return render(request, 'login.html', {
            'form': form,
            'tags': Tag.objects.popular(),
            'members': Profile.objects.popular()
        })


def logout(request):
    auth.logout(request)
    return redirect(request.GET.get('next', default='/'))


def index(request):
    page = paginate(Question.objects.new(), request)
    page_range = page.paginator.get_elided_page_range(number=page.number, on_each_side=on_each_side, on_ends=on_ends)
    return render(request, 'index.html', {'questions': page, 'authorizated': request.user.is_authenticated,
                                          'tags': Tag.objects.popular(), 'members': Profile.objects.popular(),
                                          'page_range': page_range,
                                          'next_pp': get_next_pp(page.number), 'prev_pp': get_prev_pp(page.number)})


@login_required
def one_question(request, question_id):
    page = paginate(Answer.objects.answers_for_question(question_id), request)
    num = Paginator(Answer.objects.answers_for_question(question_id), perr_page)
    page_range = page.paginator.get_elided_page_range(number=page.number, on_each_side=on_each_side, on_ends=on_ends)

    question_by_pk = get_object_or_404(Question, id=question_id)

    if request.method == 'POST' and request.user.is_authenticated:
        form = AnswerForm(profile=request.user.profile, question=question_by_pk, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('one_question', kwargs={'question_id': question_id}) + '?page=%s' % (num.num_pages))
    else:
        form = AnswerForm(profile=None, question=question_by_pk)

    return render(request, 'one_question.html', {'question': Question.objects.one_question(question_id),
                                                 'authorizated': request.user.is_authenticated,
                                                 'form': form,
                                                 'tags': Tag.objects.popular(), 'page_range': page_range,
                                                 'members': Profile.objects.popular(), 'answers': page,
                                                 'next_pp': get_next_pp(page.number),
                                                 'prev_pp': get_prev_pp(page.number)})


@login_required
def ask(request):
    if request.method == 'POST':
        form = QuestionForm(data=request.POST, profile=request.user.profile)
        if form.is_valid():
            new_question = form.save()
            return redirect(reverse('one_question', kwargs={'question_id': new_question.pk}))
    else:
        form = QuestionForm(profile=request.user.profile)

    return render(request, 'ask.html', {'tags': Tag.objects.popular(), 'members': Profile.objects.popular(), 'form': form,
                                        'authorizated': request.user.is_authenticated})


def tag(request, tag_name):
    page = paginate(Question.objects.one_tag(tag_name), request)
    page_range = page.paginator.get_elided_page_range(number=page.number, on_each_side=on_each_side, on_ends=on_ends)
    return render(request, 'one_tag.html', {'tags': Tag.objects.popular(), 'members': Profile.objects.popular(),
                                            'authorizated': request.user.is_authenticated, 'page_range': page_range,
                                            'tags_questions': page, 'tag_name': tag_name,
                                            'next_pp': get_next_pp(page.number), 'prev_pp': get_prev_pp(page.number)})


@login_required
def settings(request):
    if request.method == 'POST':
        form = SettingsForm(data=request.POST, files=request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect(reverse('settings'))
    else:
        user_data = {
            'username': request.user.username,
            'email': request.user.email,
            'avatar': request.user.profile.avatar,
        }
        form = SettingsForm(initial=user_data)

    return render(request, 'settings.html', {'tags': Tag.objects.popular(), 'members': Profile.objects.popular(),
                                             'form': form,
                                             'authorizated': request.user.is_authenticated})


def register(request):
    if request.method == 'POST':
        form = RegisterForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            user = auth.authenticate(request,
                                     username=form.cleaned_data['username'],
                                     password=form.cleaned_data['password1']
                                     )
            if user is not None:
                auth.login(request, user)
                return redirect('/')
            else:
                form.add_error(None, 'A user with that email already exists.')
        else:
            form.add_error(None, 'Sorry, password1 != password2!')
    else:
        form = RegisterForm()

    return render(request, 'register.html', {'tags': Tag.objects.popular(), 'members': Profile.objects.popular(),
                                             'form': form,
                                             'authorizated': request.user.is_authenticated})


@login_required
def hot(request):
    page = paginate(Question.objects.hot(), request)
    page_range = page.paginator.get_elided_page_range(number=page.number, on_each_side=on_each_side, on_ends=on_ends)
    return render(request, 'hot_questions.html', {'questions': page, 'authorizated': request.user.is_authenticated,
                                                  'tags': Tag.objects.popular(),
                                                  'members': Profile.objects.popular(), 'page_range': page_range,
                                                  'next_pp': get_next_pp(page.number),
                                                  'prev_pp': get_prev_pp(page.number)})


def paginate(objects_list, request, per_page=perr_page):
    paginator = Paginator(objects_list, per_page)
    page_number = request.GET.get('page')
    return paginator.get_page(page_number)


@require_POST
@login_required
def vote(request):
    data = request.POST

    if data['vote'] == 'like':
        user_vote = True
    else:
        user_vote = False

    if 'question' == data['name_class']:
        user_vote_data = {
            'user': request.user.profile,
            'is_liked': user_vote,
            'question_id': data['id'],
        }
        form = VoteQuestionForm(action=data['action'], data=user_vote_data)
        if form.is_valid():
            form.save()
        rating = Question.objects.get(id=data['id']).rating
        print(rating)

    if 'answer' == data['name_class']:
        user_vote_data = {
            'user': request.user.profile,
            'is_liked': user_vote,
            'answer_id': data['id'],
        }
        form = VoteAnswerForm(action=data['action'], data=user_vote_data)
        print(1)
        if form.is_valid():
            form.save()
            print(2)
        rating = Answer.objects.get(id=data['id']).rating
        print(rating)

    return JsonResponse({'rating': rating})


@require_POST
@login_required
def correct(request):
    data = request.POST
    form = CorrectForm(data=data)
    if form.is_valid():
        answer = form.save()
    return JsonResponse({'correct': answer.is_correct})


# for '...' in paginator
def get_next_pp(number):
    return number + on_each_side * 2


def get_prev_pp(number):
    return number - on_each_side * 2
